package main

import (
	"context"
	"flag"
	"fmt"
	client "gitlab.com/bruno-nascimento-public/go_rest_api/pkg/client/http"
	"gitlab.com/bruno-nascimento-public/go_rest_api/pkg/model"
)

func main() {
	var word string
	flag.StringVar(&word, "word", "A man, a plan, a canal: Panama!", "word/sentence to be checked to see if it is a palindrome")
	var server string
	flag.StringVar(&server, "server", "http://34.140.176.166/", "is-palindrome service address")

	flag.Parse()

	cli, err := client.NewClientWithResponses(server)
	if err != nil {
		panic(err)
	}
	response, err := cli.IsPalindromeWithResponse(context.Background(), &model.IsPalindromeParams{Word: word})
	if err != nil {
		panic(err)
	}
	println(`
░▀█▀░█▀▀░░░░░█▀█░█▀█░█░░░▀█▀░█▀█░█▀▄░█▀▄░█▀█░█▄█░█▀▀░░░█▀▀░█░░░▀█▀
░░█░░▀▀█░▄▄▄░█▀▀░█▀█░█░░░░█░░█░█░█░█░█▀▄░█░█░█░█░█▀▀░░░█░░░█░░░░█░
░▀▀▀░▀▀▀░░░░░▀░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀
`)
	println(fmt.Sprintf("WORD: \t\t%s", word))
	println(fmt.Sprintf("IS PALINDROME? \t%s", result(response.Body)))
	println(fmt.Sprintf("POD ID: \t%s", response.HTTPResponse.Header.Get(model.PodIdHeaderName)))
}

func result(body []byte) string {
	resp := string(body)
	if resp == "true" {
		return "👍 yes"
	}
	return "👎 no"
}
