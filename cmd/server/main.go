package main

import (
	"github.com/gofiber/fiber/v2/log"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/config"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/infrastructure/server"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("error loading configuration: %s", err.Error())
	}
	server.Start(cfg)
}
