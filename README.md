## Golang REST API

A simple service with only one endpoint just to check if a string is a palindrome or not.
A client is also provided.

### Getting started

Let's use the client cmd to call the service running on the provided k8s cluster:

```shell
❯ go install gitlab.com/bruno-nascimento-public/go_rest_api/cmd/is_palindrome_cli@latest  

❯ is_palindrome_cli 
░▀█▀░█▀▀░░░░░█▀█░█▀█░█░░░▀█▀░█▀█░█▀▄░█▀▄░█▀█░█▄█░█▀▀░░░█▀▀░█░░░▀█▀
░░█░░▀▀█░▄▄▄░█▀▀░█▀█░█░░░░█░░█░█░█░█░█▀▄░█░█░█░█░█▀▀░░░█░░░█░░░░█░
░▀▀▀░▀▀▀░░░░░▀░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀
WORD:           A man, a plan, a canal: Panama!
IS PALINDROME?  👍 yes
POD ID:         go-rest-api-6dd64476b-nf2qm

❯ is_palindrome_cli --word racecar 
░▀█▀░█▀▀░░░░░█▀█░█▀█░█░░░▀█▀░█▀█░█▀▄░█▀▄░█▀█░█▄█░█▀▀░░░█▀▀░█░░░▀█▀
░░█░░▀▀█░▄▄▄░█▀▀░█▀█░█░░░░█░░█░█░█░█░█▀▄░█░█░█░█░█▀▀░░░█░░░█░░░░█░
░▀▀▀░▀▀▀░░░░░▀░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀
WORD:           racecar
IS PALINDROME?  👍 yes
POD ID:         go-rest-api-6dd64476b-wvlm8

❯ is_palindrome_cli --word 'not a palindrome'
░▀█▀░█▀▀░░░░░█▀█░█▀█░█░░░▀█▀░█▀█░█▀▄░█▀▄░█▀█░█▄█░█▀▀░░░█▀▀░█░░░▀█▀
░░█░░▀▀█░▄▄▄░█▀▀░█▀█░█░░░░█░░█░█░█░█░█▀▄░█░█░█░█░█▀▀░░░█░░░█░░░░█░
░▀▀▀░▀▀▀░░░░░▀░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀
WORD:           not a palindrome
IS PALINDROME?  👎 no
POD ID:         go-rest-api-6dd64476b-nf2qm
```

Since it is a simple service, we don't have any dependencies, so if you want to test it you can simply run the server and the client:

*Server*:

`go run cmd/server/main.go`

```shell
❯ go run cmd/server/main.go
 ┌───────────────────────────────────────────────────┐ 
 │                   Fiber v2.52.2                   │ 
 │               http://127.0.0.1:8080               │ 
 │       (bound on host 0.0.0.0 and port 8080)       │ 
 │                                                   │ 
 │ Handlers ............. 9  Processes ........... 1 │ 
 │ Prefork ....... Disabled  PID ............. 99067 │ 
 └───────────────────────────────────────────────────┘ 
21:52:00 | 200 |     220.667µs | 127.0.0.1 | GET | /palindrome/v1/check | -
```

*Client*:

`go run cmd/is_palindrome_cli/main.go --server http://localhost:8080 --word 'racecar'`

```shell
❯ go run cmd/is_palindrome_cli/main.go --server http://localhost:8080 --word 'racecar'                         
░▀█▀░█▀▀░░░░░█▀█░█▀█░█░░░▀█▀░█▀█░█▀▄░█▀▄░█▀█░█▄█░█▀▀░░░█▀▀░█░░░▀█▀
░░█░░▀▀█░▄▄▄░█▀▀░█▀█░█░░░░█░░█░█░█░█░█▀▄░█░█░█░█░█▀▀░░░█░░░█░░░░█░
░▀▀▀░▀▀▀░░░░░▀░░░▀░▀░▀▀▀░▀▀▀░▀░▀░▀▀░░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀▀
WORD:           racecar
IS PALINDROME?  👍 yes
POD ID:         notebook.home
```

or just cURL it, if you prefer: 

```shell
❯ curl 'http://localhost:8080/palindrome/v1/check?word=racecar' -v  
*   Trying [::1]:8080...
* connect to ::1 port 8080 failed: Connection refused
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080
> GET /palindrome/v1/check?word=racecar HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.4.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Sun, 17 Mar 2024 22:16:58 GMT
< Content-Type: application/json
< Content-Length: 4
< Pod-Id: notebook.home
< 
* Connection #0 to host localhost left intact
true
```

### Run and deploy the service in a local K8S cluster using *Kind*

1. Install kind:                     
`go install sigs.k8s.io/kind@v0.22.0`
2. Create a cluster:                 
`kind create cluster --config deploy/local/kind.yml`
3. Install Nginx Ingress Controller: 
`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml`
4. Deploy the application:           
`sed "s/\$VERSION/latest/" deploy/deployment.yaml | kubectl apply -f -`

> If during the step #4 you see the following error message:
> 
> `Error from server (InternalError): error when creating "deploy/deployment.yaml": Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": failed to call webhook:`
> 
> Please run this command and try again:
>  `kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission`

### Server environment variables

| environment variable      | description                                                                       | default value         |
|---------------------------|-----------------------------------------------------------------------------------|-----------------------|
| APP_NAME                  | service name                                                                      | is-palindrome-service |
| HTTP_PORT                 | http port where the service would listen for incoming requests                    | 8080                  |
| HTTP_SHUTDOWN_TIMEOUT     | interval in seconds for graceful shutdown timeout                                 | 5s                    |
| PALINDROME_NORMALIZE_WORD | a flag to tell the service if the words being checked should be normalized or not | true                  |

### K8S dev cluster configuration

> I had plans to turn this into a [job that run manually](https://docs.gitlab.com/ee/ci/jobs/job_control.html#create-a-job-that-must-be-run-manually) to be used in GitLab-CI.<br/>
> I am sorry, but for now let's put it on the backlog to work on this in the next sprint ;)

```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true
```

### Generating server, client and types using openapi
You can find the spec [here](internal/entrypoints/http/api/is_palindrome.openapi.yaml)
```shell
make generate
```
this will generate:
- the models in `pkg/client/http/client.gen.go`
- the http client in `pkg/client/http/client.gen.go`
- the server in `internal/entrypoints/http/api/is-palindrome-server.gen.go`

If you make changes to the openapi spec, run the generate command, run the unit tests and commit the changes. 

### TODO<font size="2">s</font> left behind <font size="2"> aka 'the backlog'</font> 

Of course, a lot of shortcuts where taken. The time to dedicate to this code challenge was limited, so I had to prioritize the tasks.

My todo list of things I left behind:
 - Install nginx ingress using a manual activated pipeline
 - Unit test coverage
 - A helm chart for the golang rest api
 - Remove the `/palindrome` with URL rewrite annotation on ingress configuration to make the URL shorter
 - Use rules to control when to run each stage e.g: configure the pipeline to only run tests when opening a merge request
 - Maybe create an ephemeral cluster when opening a Merge Request? To let developer validate their changes in isolation. That would be a dream not a todo :)

Thank you!<br/>
brunocn80@gmail.com