install_deps:
	go install github.com/deepmap/oapi-codegen/v2/cmd/oapi-codegen@v2.1.0
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.56.2

lint:
	golangci-lint run ./... -v

lint-ci:
	golangci-lint run ./... --out-format=github-actions --timeout=5m

generate:
	go generate ./...

test:
	go test -cover ./...

tidy:
	go mod tidy

tidy-ci:
	tidied -verbose