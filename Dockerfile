FROM golang:1.22-alpine as builder

WORKDIR /app
COPY . .
RUN go mod download && go mod verify
RUN go build --ldflags "-s -w" -v -o /usr/local/bin/app ./cmd/server

FROM scratch
COPY --from=builder /usr/local/bin/app /usr/local/bin/app
CMD ["/usr/local/bin/app"]