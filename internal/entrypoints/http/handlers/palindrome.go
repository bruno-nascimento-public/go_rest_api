package handlers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/entrypoints/http/api"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/usecase"
	"gitlab.com/bruno-nascimento-public/go_rest_api/pkg/model"
	"net/http"
)

type Palindrome struct {
	useCase usecase.PalindromeUseCase
}

func New(useCase usecase.PalindromeUseCase) api.ServerInterface {
	return &Palindrome{useCase}
}

func (p *Palindrome) IsPalindrome(ctx *fiber.Ctx, params model.IsPalindromeParams) error {
	return ctx.Status(http.StatusOK).JSON(p.useCase.IsPalindrome(params.Word))
}
