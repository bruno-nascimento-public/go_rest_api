package handlers

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
)

func RegisterProbes(router fiber.Router) {
	router.Get("/live", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(http.StatusOK)
	})

	router.Get("/ready", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(http.StatusOK)
	})
}
