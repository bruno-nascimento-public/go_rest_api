//go:generate oapi-codegen --config=types.cfg.yaml is_palindrome.openapi.yaml
//go:generate oapi-codegen --config=server.cfg.yaml is_palindrome.openapi.yaml
//go:generate oapi-codegen --config=client.cfg.yaml is_palindrome.openapi.yaml
package api
