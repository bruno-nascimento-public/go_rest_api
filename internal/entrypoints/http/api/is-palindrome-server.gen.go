// Package api provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen/v2 version v2.1.0 DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/gofiber/fiber/v2"
	"github.com/oapi-codegen/runtime"
	. "gitlab.com/bruno-nascimento-public/go_rest_api/pkg/model"
)

// ServerInterface represents all server handlers.
type ServerInterface interface {

	// (GET /palindrome/v1/check)
	IsPalindrome(c *fiber.Ctx, params IsPalindromeParams) error
}

// ServerInterfaceWrapper converts contexts to parameters.
type ServerInterfaceWrapper struct {
	Handler ServerInterface
}

type MiddlewareFunc fiber.Handler

// IsPalindrome operation middleware
func (siw *ServerInterfaceWrapper) IsPalindrome(c *fiber.Ctx) error {

	var err error

	// Parameter object where we will unmarshal all parameters from the context
	var params IsPalindromeParams

	var query url.Values
	query, err = url.ParseQuery(string(c.Request().URI().QueryString()))
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, fmt.Errorf("Invalid format for query string: %w", err).Error())
	}

	// ------------- Required query parameter "word" -------------

	if paramValue := c.Query("word"); paramValue != "" {

	} else {
		err = fmt.Errorf("Query argument word is required, but not found")
		c.Status(fiber.StatusBadRequest).JSON(err)
		return err
	}

	err = runtime.BindQueryParameter("form", true, true, "word", query, &params.Word)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, fmt.Errorf("Invalid format for parameter word: %w", err).Error())
	}

	return siw.Handler.IsPalindrome(c, params)
}

// FiberServerOptions provides options for the Fiber server.
type FiberServerOptions struct {
	BaseURL     string
	Middlewares []MiddlewareFunc
}

// RegisterHandlers creates http.Handler with routing matching OpenAPI spec.
func RegisterHandlers(router fiber.Router, si ServerInterface) {
	RegisterHandlersWithOptions(router, si, FiberServerOptions{})
}

// RegisterHandlersWithOptions creates http.Handler with additional options
func RegisterHandlersWithOptions(router fiber.Router, si ServerInterface, options FiberServerOptions) {
	wrapper := ServerInterfaceWrapper{
		Handler: si,
	}

	for _, m := range options.Middlewares {
		router.Use(m)
	}

	router.Get(options.BaseURL+"/palindrome/v1/check", wrapper.IsPalindrome)

}

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/6xUwW7jNhD9lcG0wKIAY9nOJdCpKbAocmmD7d42OYypscREHDJDKl4jMNB/6B/2SwpS",
	"TuzFBuhlTyLI4ePTvPfmBW3wMQhLTti+YLIDe6rLj6pByyJqiKzZcd22oePyzfvI2KKTzD0rHgx6Ton6",
	"88OU1UmPh4NB5afJKXfYfpkhTvX35rU+bB7Y5oL1idM05jOoTQgjk+ChgDnZhpmLZLK1jD25sdTpJMHK",
	"1fLXvuwsbPBoUMgXkN/KIfxByTrPkgManLTcGnKOqW2a3W63GJ08cuekXG2cNBXxwtLYsVO66MKFnAAO",
	"BjtOVl3MLgi2eA19GEl6+PTxr89wfXsDO5cHCDLuIQgDSxeDkwwPU8qQA9iB7SO4LRDM7QKXgCDS6KTT",
	"4BmCgoS8QIPZ5bH8x+9/vsGjwWfWND++XKwWy8IpRBaKDlu8XCwXSzQYKQ9Vv+aE3Dyvmvp82e+59vHb",
	"v/k8uDTzeeOdAyTn47g/Z967Z5b/4X8nWIkpFfCbDlu8SbdvZZWjkufMmrD98j0Vhl3QzoBMfsNqIA5K",
	"iU3BD3lghcRPE4tlCFtIe78JY/qmwy6DMnUJ8sCQyDNsyD7uSLsElGAbtK4NpMkOZcdTR77gK1m2pKbe",
	"7CgzkHSQnWdYrZv1qrm8hNW6Xa/MfFDwWXIlc4fX4ElMaco4fy0JjfDv3//ALQl5usOiLn8lH6u+x+ew",
	"OB1bfJpY9ycXly7geaKyTmyO0X0vffelOMUgaY7werl8TQ9LVZ1iHJ2twjQPqfT75QzvZ+UttvhTc5oV",
	"zXFQNMeg1lh+L5jW06JHackHl8588WEWZo7Qlo5x/yGk5tH1DqdJ+Gtkm7kDPtYYzNQXw+HJixezZd48",
	"3/6C9xUtsT6/2vM0OdqmGYOlcQgpt1fLq2WDh/vDfwEAAP//z2SwIVsFAAA=",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %w", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	res := make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	resolvePath := PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		pathToFile := url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
