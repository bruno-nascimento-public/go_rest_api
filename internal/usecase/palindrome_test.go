package usecase

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/config"
	"testing"
)

func TestPalindrome_IsPalindrome(t *testing.T) {
	cfgNormalizationOff, err := config.NewMock(map[string]string{"PALINDROME_NORMALIZE_WORD": "false"})
	require.NoError(t, err)
	cfgNormalizationOn, err := config.New()
	require.NoError(t, err)

	type fields struct {
		cfg *config.Config
	}
	type args struct {
		word string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "OK_1991",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "1991"},
			want:   true,
		},
		{
			name:   "FAIL_1990",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "1990"},
			want:   false,
		},
		{
			name:   "OK_RaCe_CaR_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "RaCe_CaR"},
			want:   true,
		},
		{
			name:   "FAIL_RaCe_CaR_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "RaCe_CaR"},
			want:   false,
		},
		{
			name:   "OK_RaCe_CaR!_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "RaCe CaR!"},
			want:   true,
		},
		{
			name:   "FAIL_!RaCe_CaR_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "!RaCe CaR"},
			want:   false,
		},
		{
			name:   "OK_Was it a car or a cat I saw?_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "Was it a car or a cat I saw?"},
			want:   true,
		},
		{
			name:   "OK_Do geese see God?_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "Do geese see God?"},
			want:   true,
		},
		{
			name:   "OK_Are we not drawn onward, we few, drawn onward to new era?_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "Are we not drawn onward, we few, drawn onward to new era?"},
			want:   true,
		},
		{
			name:   "OK_He won a toyota now, eh_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "He won a toyota now, eh!"},
			want:   true,
		},
		{
			name:   "OK_detartrated_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "detartrated"},
			want:   true,
		},
		{
			name:   "OK_detartrated_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "detartrated"},
			want:   true,
		},
		{
			name:   "OK_Rotavator_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "Rotavator"},
			want:   true,
		},
		{
			name:   "OK_Rotavator_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "Rotavator"},
			want:   true,
		},
		{
			name:   "OK_aippuakivikauppias_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "saippuakivikauppias"},
			want:   true,
		},
		{
			name:   "OK_aippuakivikauppias_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "saippuakivikauppias"},
			want:   true,
		},
		{
			name:   "OK_A man, a plan, a canal: Panama!_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "A man, a plan, a canal: Panama!"},
			want:   true,
		},
		{
			name:   "FAIL_A man, a plan, a canal: Panama!_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "A man, a plan, a canal: Panama!"},
			want:   false,
		},
		{
			name:   "OK_02/02/2020_NORMALIZATION_ON",
			fields: fields{cfgNormalizationOn},
			args:   args{word: "02/02/2020"},
			want:   true,
		},
		{
			name:   "FAIL_02/02/2020_NORMALIZATION_OFF",
			fields: fields{cfgNormalizationOff},
			args:   args{word: "02/02/2020"},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPalindromeUseCase(tt.fields.cfg)
			if got := p.IsPalindrome(tt.args.word); got != tt.want {
				t.Errorf("IsPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
