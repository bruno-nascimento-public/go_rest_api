package usecase

import (
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/config"
	"regexp"
	"strings"
)

type (
	PalindromeUseCase interface {
		IsPalindrome(word string) bool
	}

	Palindrome struct {
		cfg                  *config.Config
		nonAlphanumericRegex *regexp.Regexp
	}
)

func NewPalindromeUseCase(cfg *config.Config) PalindromeUseCase {
	return &Palindrome{cfg: cfg, nonAlphanumericRegex: regexp.MustCompile(`[^\p{L}\p{N}]+`)}
}

func (p *Palindrome) IsPalindrome(word string) bool {
	original := strings.ToLower(word)
	if p.cfg.PALINDROME.NormalizeWord {
		original = p.nonAlphanumericRegex.ReplaceAllString(original, "")
		if original == "" {
			return false
		}
	}
	for i, j := 0, len(original)-1; i < j; i, j = i+1, j-1 {
		if original[i] != original[j] {
			return false
		}
	}
	return true
}

// same results as the for above but spending more time and resources
// reversedArr := strings.Split(original, "")
// slices.Reverse(reversedArr)
// reverseString := strings.Join(reversedArr, "")
// return original == reverseString
