package server

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/config"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/entrypoints/http/handlers"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/usecase"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
)

func TestPalindromeV1CheckRoute(t *testing.T) {
	tests := []struct {
		word     string
		response bool
	}{
		{
			word:     "1991",
			response: true,
		},
		{
			word:     "1992",
			response: false,
		},
		{
			word:     "RaCe_CaR",
			response: true,
		},
		{
			word:     "RaCe CaR!",
			response: true,
		},
		{
			word:     "Was it a car or a cat I saw?",
			response: true,
		},
		{
			word:     "This is not a palindrome",
			response: false,
		},
	}

	cfg, err := config.New()
	assert.NoError(t, err)
	app := NewFiberServer(handlers.New(usecase.NewPalindromeUseCase(cfg)))

	for _, test := range tests {
		t.Run(test.word, func(t *testing.T) {
			req := httptest.NewRequest("GET", fmt.Sprintf("/palindrome/v1/check?word=%s", url.QueryEscape(test.word)), nil)
			resp, err := app.Test(req, -1)
			assert.NoError(t, err)
			bodyBytes, err := io.ReadAll(resp.Body)
			assert.NoError(t, err)
			isPalindrome, err := strconv.ParseBool(string(bodyBytes))
			assert.NoError(t, err)
			assert.Equal(t, resp.StatusCode, http.StatusOK)
			assert.Equal(t, test.response, isPalindrome)
		})
	}
}
