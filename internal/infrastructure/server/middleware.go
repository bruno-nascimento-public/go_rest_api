package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"gitlab.com/bruno-nascimento-public/go_rest_api/pkg/model"
	"os"
)

func PodIDMiddleware() fiber.Handler {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal("could not get the hostname in pod id middleware: %s", err.Error())
	}
	return func(c *fiber.Ctx) error {
		c.Set(model.PodIdHeaderName, hostname)
		return c.Next()
	}
}
