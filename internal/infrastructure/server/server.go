package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/log"
	"github.com/gofiber/fiber/v2/middleware/logger"
	middleware "github.com/oapi-codegen/fiber-middleware"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/config"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/entrypoints/http/api"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/entrypoints/http/handlers"
	"gitlab.com/bruno-nascimento-public/go_rest_api/internal/usecase"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func Start(cfg *config.Config) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	s := NewFiberServer(handlers.New(usecase.NewPalindromeUseCase(cfg)))
	go func() {
		if err := s.Listen(net.JoinHostPort("0.0.0.0", "8080")); err != nil {
			log.Error(err.Error())
		}
	}()

	killSignal := <-interrupt
	switch killSignal {
	case os.Interrupt:
		log.Warn("Got SIGINT...")
	case syscall.SIGTERM:
		log.Warn("Got SIGTERM...")
	}

	log.Info("The service is shutting down...")
	err := s.ShutdownWithTimeout(cfg.HTTP.ShutdownTimeout)
	if err != nil {
		log.Errorf("error shutting down the server: %s", err.Error())
		return
	}
	log.Info("Done. BYE!")
}

func NewFiberServer(handler api.ServerInterface) *fiber.App {
	swagger, err := api.GetSwagger()
	if err != nil {
		log.Fatalf("Error loading swagger spec: %s", err.Error())
	}
	swagger.Servers = nil

	app := fiber.New()
	app.Use(logger.New())
	app.Use(PodIDMiddleware())
	handlers.RegisterProbes(app)

	app.Use(middleware.OapiRequestValidator(swagger)) // this must be AFTER the handlers.RegisterProbes call
	api.RegisterHandlers(app, handler)

	return app
}
