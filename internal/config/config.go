package config

import (
	"context"
	"github.com/sethvargo/go-envconfig"
	"time"
)

type Config struct {
	APP struct {
		Name string `env:"APP_NAME,default=is-palindrome-service"`
	}
	HTTP struct {
		Port            string        `env:"HTTP_PORT,default=:8080"`
		ShutdownTimeout time.Duration `env:"HTTP_SHUTDOWN_TIMEOUT,default=5s"`
	}
	PALINDROME struct {
		NormalizeWord bool `env:"PALINDROME_NORMALIZE_WORD,default=true"`
	}
}

func New() (*Config, error) {
	var config Config
	if err := envconfig.Process(context.Background(), &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func NewMock(mapper map[string]string) (*Config, error) {
	var config Config
	err := envconfig.ProcessWith(context.Background(), &envconfig.Config{
		Target:   &config,
		Lookuper: envconfig.MapLookuper(mapper),
	})
	if err != nil {
		return nil, err
	}
	return &config, nil
}
