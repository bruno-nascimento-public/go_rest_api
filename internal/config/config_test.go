package config

import (
	"reflect"
	"testing"
)

func TestLoadFromEnv(t *testing.T) {
	expectedAppId := "app_id"
	t.Setenv("APP_NAME", expectedAppId)
	config, err := New()
	if err != nil {
		t.Fatalf("error creating new config: %s", err.Error())
	}
	if !reflect.DeepEqual(config.APP.Name, expectedAppId) {
		t.Fatalf("we were expecting the APP.NAME prop to had an '%s' value but we got : %s", expectedAppId, config.APP.Name)
	}
}

func TestDefault(t *testing.T) {
	config, err := New()
	if err != nil {
		t.Fatalf("error creating new config: %s", err.Error())
	}
	expected := "is-palindrome-service"
	if config.APP.Name != expected {
		t.Fatalf("we were expecting the APP.TeamsList prop to had an '%s' value but we got : %s", expected, config.APP.Name)
	}
}
